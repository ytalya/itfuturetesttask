package com.example.client;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

@SpringBootApplication
public class ClientApplication {
    private static final int threadCount = 3;   // - количество клиентских потоков (>= 1)
    private static final int readQuota = 5;     // - доля запросов getBalance (>= 0)
    private static final int writeQuota = 5;    // - доля запросов changeBalance (>= 0)
    private static final List<Long> readIdList = Arrays.asList(1L, 2L, 3L, 6L, 888L, 555L);
    private static final List<Long> writeIdList = Arrays.asList(1L, 2L, 3L, 6L, 888L, 555L);
    public static void main(String[] args) {

        ExecutorService threadPool = Executors.newFixedThreadPool(threadCount);
        Runnable r = () -> {
            while (true) {
                double readProbability = (double)readQuota/(double)(readQuota + writeQuota);
                double randomDoubleValue = ThreadLocalRandom.current().nextDouble();
                if (randomDoubleValue < readProbability) {
                    getBalance(randomFromList(readIdList));
                } else {
                    changeBalance(randomFromList(writeIdList), 1L);
                }
            }
        };

        for(int i = 0; i < threadCount; i++){
            threadPool.execute(r);
        }

        //Для оценки производительности сервиса нужно реализовать клиента,
        //который в несколько потоков выполняет бесконечный цикл
        //threadPool.shutdown();
        //threadPool.awaitTermination(10, TimeUnit.SECONDS);
    }
    private static void changeBalance(Long accountNumber, Long  accountValue) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            String resourceUrl = "http://localhost:8085/balance/changeBalance?id=" + accountNumber + "&value=" + accountValue;
            ResponseEntity<String> response = restTemplate.postForEntity(resourceUrl, "", String.class);
        }catch (HttpClientErrorException e){
            System.out.println("Send message error: " + e.getResponseBodyAsString());
        }
        catch (RestClientResponseException | ResourceAccessException e) {
            System.out.println("Failed to get remote resource because: " + e.getMessage());
        }
    }
    private static void  getBalance(Long accountNumber) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            String resourceUrl = "http://localhost:8085/balance/by-id?id=" + accountNumber;
            ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);
        }catch (HttpClientErrorException  e){
            System.out.println("Send message error: " + e.getResponseBodyAsString());
        }catch (RestClientResponseException | ResourceAccessException e) {
            System.out.println("Failed to get remote resource because: " + e.getMessage());
        }
    }
    private static Long randomFromList(List<Long> readIdList) {
        Random rand = new Random();
        Long randomElement = readIdList.get(rand.nextInt(readIdList.size()));
        return randomElement;
    }
}