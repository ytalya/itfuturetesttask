package com.example.server.err;

public class BalanceApiWrongParameterException extends Exception {
    public BalanceApiWrongParameterException(String message) {
        super(message);
    }
}
