package com.example.server.services;

import com.example.server.entities.Balance;
import com.example.server.err.BalanceApiWrongParameterException;
import com.example.server.repositories.BalanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;

@Service
public class BalanceService {
    BalanceRepository balanceRepository;

    @Autowired
    public BalanceService(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }
    @Cacheable("balances")
    public Balance balanceById(Long number) throws BalanceApiWrongParameterException {
        return Optional.ofNullable(balanceRepository.findBalanceByNumber(number))
                .orElseThrow(() -> new BalanceApiWrongParameterException("Balance not found by id " + number));
    }
    @Transactional
    public Balance changeBalance(Long number, Long extra_amount) throws ObjectOptimisticLockingFailureException {
        return Optional.ofNullable(balanceRepository.findBalanceByNumber(number)).map(balance -> {
                    balance.setAmount(balance.getAmount() + extra_amount);
                    balanceRepository.save(balance);
                    return balance;
                })
                .orElseGet(() -> Optional.ofNullable(new Balance()).map(balance -> {
                    balance.setNumber(number);
                    balance.setAmount(extra_amount);
                    balanceRepository.save(balance);
                    return balance;
                }).orElseThrow());
   }
}
