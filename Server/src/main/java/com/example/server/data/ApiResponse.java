package com.example.server.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Collection;

public class ApiResponse<T> {
    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status=status;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp=timeStamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message=message;
    }

    public String getDebugMessage() {
        return debugMessage;
    }

    public void setDebugMessage(String debugMessage) {
        this.debugMessage=debugMessage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data=data;
    }

    private HttpStatus status;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timeStamp;

    private String message;
    private String debugMessage;
    private T data;

    public ApiResponse() {
        this.timeStamp=LocalDateTime.now();
    }

    public ApiResponse(HttpStatus status, LocalDateTime timeStamp, String message, String debugMessage, T data) {
        this.status=status;
        this.timeStamp=timeStamp;
        this.message=message;
        this.debugMessage=debugMessage;
        this.data=data;
    }

    public ApiResponse(HttpStatus status, String message, Throwable ex) {
        this();
        this.status=status;
        this.message=message;
        this.debugMessage = ex.getLocalizedMessage();
    }
}
