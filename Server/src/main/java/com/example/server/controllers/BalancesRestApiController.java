package com.example.server.controllers;

import com.example.server.data.ApiResponse;
import com.example.server.entities.Balance;
import com.example.server.err.BalanceApiWrongParameterException;
import com.example.server.services.BalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/balance")
public class BalancesRestApiController {

    private final BalanceService balanceService;

    @Autowired
    public BalancesRestApiController(BalanceService balanceService) {
        this.balanceService = balanceService;
    }

    @GetMapping("/by-id")
    public ResponseEntity<ApiResponse<Balance>> balanceById(@RequestParam("id")Long id) throws BalanceApiWrongParameterException, InterruptedException {
        ApiResponse<Balance> response = new ApiResponse<>();
        Balance balance = balanceService.balanceById(id);
        response.setDebugMessage("Successful request");
        response.setMessage("Money in the account: " + balance.getNumber() + " - " + balance.getAmount());
        response.setTimeStamp(LocalDateTime.now());
        response.setData(balance);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/changeBalance")
    public ResponseEntity<ApiResponse<Balance>> changeBalance(@RequestParam("id") Long id, @RequestParam("value") Long value) throws InterruptedException {
        ApiResponse<Balance> response = new ApiResponse<>();
        Balance balance = new Balance();

        try {
            balance = balanceService.changeBalance(id, value);
        } catch (ObjectOptimisticLockingFailureException exception_first) {
            try {
                balance = balanceService.changeBalance(id, value);
            } catch (ObjectOptimisticLockingFailureException exception_second) {
                System.out.println("Optimistic Locking Failure: " + exception_second.getMessage());
            }
        }
        response.setDebugMessage("Successful request");
        response.setMessage("Account is created or updated: " + balance.getNumber() + " - " + balance.getAmount());
        response.setTimeStamp(LocalDateTime.now());
        response.setData(balance);
        return ResponseEntity.ok(response);
    }

    @ExceptionHandler(BalanceApiWrongParameterException.class)
    public ResponseEntity<ApiResponse<Balance>> handleBalanceApiWrongParameterException(Exception exception){
        return new ResponseEntity<>(new ApiResponse<Balance>(HttpStatus.BAD_REQUEST, "Bad parameter value...", exception),
                HttpStatus.BAD_REQUEST);
    }











}
