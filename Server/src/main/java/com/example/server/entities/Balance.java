package com.example.server.entities;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name="balances")
public class Balance{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(unique=true)
    @Min(value = 1, message = "Account number must be gt 0")
    private Long number;

    @Min(value = 0, message = "Account amount must be positive value")
    private Long amount;

    //We should remember that for versioned entities optimistic locking is available by default.
    @Version
    private Integer version;
}
