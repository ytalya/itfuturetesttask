package com.example.server.repositories;

import com.example.server.entities.Balance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BalanceRepository extends JpaRepository<Balance, Integer> {
    Balance findBalanceByNumber(Long number);
}
