package com.example.server.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

@Aspect
@Component
public class MethodActionTracker {
    private static final long SECOND = 1000;
    private static final long MINUTE = SECOND * 60;
    private final Logger logger =Logger.getLogger(this.getClass().getName());
    private static volatile AtomicInteger balanceByIdCounter = new AtomicInteger(0);
    private static volatile AtomicInteger changeBalanceCounter = new AtomicInteger(0);
    private static Long startTimeMills;

    static {
        startTimeMills = new Date().getTime();
    }

    @Before(value = "execution(* com.example.server.controllers.BalancesRestApiController.changeBalance(..))")
    public void changeBalanceTrackableAdvice() {
        changeBalanceCounter.getAndIncrement();
        printPerformance();
    }

    @Before(value = "execution(* com.example.server.controllers.BalancesRestApiController.balanceById(..))")
    public void balanceByIdTrackableAdvice() {
        balanceByIdCounter.getAndIncrement();
        printPerformance();
    }
    synchronized private void printPerformance(){
        Long duration = new Date().getTime() - startTimeMills;
        Long performance = (MINUTE * (changeBalanceCounter.get() + balanceByIdCounter.get())) / duration;
        logger.info("ChangeBalanceCounter: " + changeBalanceCounter
                + " BalanceByIdCounter: " + balanceByIdCounter
                + " Average performance from start time: " +  performance + " op/min" + " duration: " + duration);
    }
}
