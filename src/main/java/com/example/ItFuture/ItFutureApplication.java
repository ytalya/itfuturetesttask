package com.example.ItFuture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItFutureApplication {
	public static void main(String[] args) {
		SpringApplication.run(ItFutureApplication.class, args);
	}
}
